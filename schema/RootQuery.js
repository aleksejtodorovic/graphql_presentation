const graphql = require('graphql');
const { GraphQLObjectType, GraphQLNonNull, GraphQLID, GraphQLList } = graphql;

const { AccountType, ExtensionType } = require('./types');
const dbApi = require("../dbApi");

const RootQuery = new GraphQLObjectType({
    name: "RootQuery",
    fields: () => ({
        account: {
            type: AccountType,
            args: {
                id: { type: GraphQLNonNull(GraphQLID) }
            },
            async resolve(parentValue, { id }) {
                const response = await dbApi.get(`accounts/${id}`);

                return response.data;
            }
        },
        accounts: {
            type: GraphQLList(AccountType),
            async resolve() {
                const response = await dbApi.get(`accounts`);

                return response.data;
            }
        },
        extension: {
            type: ExtensionType,
            args: {
                id: { type: GraphQLNonNull(GraphQLID) }
            },
            async resolve(parentValue, { id }) {
                const response = await dbApi.get(`extensions/${id}`);

                return response.data;
            }
        },
        extensions: {
            type: GraphQLList(ExtensionType),
            async resolve() {
                const response = await dbApi.get(`extensions`);

                return response.data;
            }
        }
    })
});

module.exports = RootQuery;