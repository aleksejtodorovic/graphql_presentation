const graphql = require('graphql');
const { GraphQLObjectType, GraphQLString, GraphQLID, GraphQLList, GraphQLInt } = graphql;

const dbApi = require('../dbAPI');

const AccountType = new GraphQLObjectType({
    name: "AccountType",
    fields: () => ({
        id: { type: GraphQLID },
        email: { type: GraphQLString },
        password: { type: GraphQLString },
        extensions: {
            type: GraphQLList(ExtensionType),
            async resolve(parentValue, args) {
                const response = await dbApi.get(`accounts/${parentValue.id}/extensions`);

                return response.data;
            }
        }
    })
});

const ExtensionType = new GraphQLObjectType({
    name: "ExtensionType",
    fields: () => ({
        id: { type: GraphQLID },
        title: { type: GraphQLString },
        guid: { type: GraphQLString },
        userbase: { type: GraphQLInt },
        status: { type: GraphQLString },
        account: {
            type: AccountType,
            async resolve(parentValue, args) {
                const response = await dbApi.get(`accounts/${parentValue.accountId}`);

                return response.data;
            }
        }
    })
});

module.exports = {
    AccountType,
    ExtensionType
}