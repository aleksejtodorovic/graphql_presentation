const graphql = require('graphql');
const { GraphQLSchema } = graphql;

const RootQuery = require('./RootQuery');
const RootMutation = require('./RootMutation');

module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation: RootMutation
});