const graphql = require('graphql');
const { GraphQLObjectType, GraphQLNonNull, GraphQLID, GraphQLList, GraphQLString, GraphQLInt } = graphql;

const { AccountType, ExtensionType } = require('./types');
const dbApi = require("../dbApi");

const RootQuery = new GraphQLObjectType({
    name: "RootMutation",
    fields: () => ({
        addAccount: {
            type: AccountType,
            args: {
                email: { type: GraphQLNonNull(GraphQLString) },
                password: { type: GraphQLNonNull(GraphQLString) }
            },
            async resolve(parentValue, { email, password }) {
                const response = await dbApi.post(`accounts`, {
                    email,
                    password
                });

                return response.data;
            }
        },
        addExtension: {
            type: ExtensionType,
            args: {
                title: { type: GraphQLNonNull(GraphQLString) },
                guid: { type: GraphQLNonNull(GraphQLString) },
                accountId: { type: GraphQLNonNull(GraphQLID) },
                userbase: { type: GraphQLInt },
                status: { type: GraphQLString }
            },
            async resolve(parentValue, { title, guid, accountId, userbase = 0, status = 'active' }) {
                const response = await dbApi.post('extensions', {
                    title,
                    guid,
                    userbase,
                    status,
                    accountId
                });

                return response.data;
            }
        }
    })
});

module.exports = RootQuery;